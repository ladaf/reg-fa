#include "display.h"

info_display::begin() {
  //inicializuji displej
  //Wire.setClock(400000);

  Adafruit_SSD1306::begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  clearDisplay();
  display();

  setTextSize(2);
  setTextColor(WHITE);


}

void info_display::units(char a, char b, char c, char d){
  setCursor(116, 0);
  print(a);
  setCursor(116, 16);
  print(b);
  setCursor(116, 32);
  print(c);
  setCursor(116, 48);
  print(d); 
}

void info_display::units(char *a, char *b, char *c, char *d){
  byte pozA = 128-12*strlen(a);
  setCursor(pozA, 0);
  print(a);
  
  byte pozB = 128-12*strlen(b);
  setCursor(pozB, 16);
  print(b);

  byte pozC = 128-12*strlen(c);
  setCursor(pozC, 32);
  print(c);

  byte pozD = 128-12*strlen(d);
  setCursor(pozD, 48);
  print(d); 
}


void info_display::quantities(char *a, char *b, char *c, char *d){
  setCursor(0, 0);
  print(a);
  setCursor(0, 16);
  print(b);
  setCursor(0, 32);
  print(c);
  setCursor(0, 48);
  print(d); 
}
  
info_display::tSetView(float Tpoz, float Tmer, float vlh, float amb){
  //premazu displej
  fillRect(0, 0, 128, 64, BLACK);
  
  //napisu si veliciny
  quantities("Poz", "Akt", "Vlh", "Amb");
  
  //pripravim si data pro diplej
  char poz_disp [6];
  dtostrf(Tpoz, 5, 1, poz_disp);
  char akt_disp [6];
  dtostrf(Tmer, 5, 1, akt_disp);
  char vlh_disp [6];
  dtostrf(vlh, 5, 1, vlh_disp);
  char amb_disp [6];
  dtostrf(amb, 5, 1, amb_disp);


  
  //a napisu neove hodnoty
  setCursor(50, 0);
  print(poz_disp);
  setCursor(50, 16);
  print(akt_disp);
  setCursor(50, 48);
  print(amb_disp);

  setCursor(50, 32);
  print(vlh_disp);

  units('C', 'C', '%', 'C');
  
  
  display();
}


info_display::standView(float con, float rvz, float rak, float rh){
  //premazu displej
  fillRect(0, 0, 128, 64, BLACK);
  
  //napisu si veliciny
  quantities("Con", "Rvz", "Rak", "Rh");
  
  //pripravim si data pro diplej
  char con_disp [3];
  dtostrf(con, 2,0, con_disp);
  char rvz_disp [5];
  dtostrf(rvz/1000., 4, 2, rvz_disp);
  char rak_disp [5];
  dtostrf(rak/1000., 4, 2, rak_disp);
  char rh_disp [6];
  dtostrf(rh, 5, 1, rh_disp);


  
  //a napisu neove hodnoty
  setCursor(50, 0);
  print(con_disp);
  setCursor(50, 16);
  print(rvz_disp);
  setCursor(50, 32);
  print(rak_disp);
  setCursor(42, 48);
  print(rh_disp);



  units("ppm", "kO", "kO", "%");

  
  display();
}
