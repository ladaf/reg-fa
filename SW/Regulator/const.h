#ifndef const_h
#define const_h

#include <arduino.h>

//global configuration
const float T_max = 450;
const float T_min = 50;
const int loop_delay= 10;
const float filter_c = 0.25;         //Exponential IIR filter coefficient

const float power_voltage_coeff = 5*12/(1023*4,15);
const byte power_voltage_limit = 8;

//sensoru and wire calibration
const float R0 = 14.3;
const float Rv = 0.;

//INA3221 calibration
const float U_cal = 1.f;
const float I_cal = 1.f;
const float R_shunt = 0.2f;       //ohm

//PID parameters
const float r0=3.0f;
const float Ti = 1.0f;
const float Td = 0.25f;

//Layer parameters
const float layer_shunt = 4.7e3f;
const float layer_division_coeff = 0.5f;
//concentration is calculated by formula: a*ln(b*r)+c
//r is resistance ration
//below enter calibraton constants a, b, c
const float concA = 4.87917914f;


//pin configuration
const byte INA_ch = 3;

#define W_pin A0
#define PWR_U_pin A1

#define I_SENS_pin A7
#define U_SENS_pin A6
#define LAYER_CURR_PWM_pin 12

#define DHT_pin 7

#define SW_A_pin 8
#define SW_B_pin 9
#define SW_C_pin 10

#define JMP_A_pin 3
#define JMP_B_pin 4
#define JMP_C_pin 5


#endif
