#ifndef ohmmeter_h
#define ohmmeter_h

#include <SDL_Arduino_INA3221.h> 


class INA_ohmmeter:public SDL_Arduino_INA3221{
  private:
    
  public:
    float getVoltage();
    float getCurrent();
    float getResistance();

    void printDebugInfo();
};

#endif
