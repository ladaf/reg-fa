#ifndef pid_h
#define pid_h

#include "arduino.h"

class PID_regulator {
  public:
    PID_regulator();
    
    void setParameters(float r0, float Ti, float Td);
    void setW(float new_w);
    float evaluate(float y);
    void debug(byte);
  private:
    float w;
    float v;
    float y;
    
    float r0;
    float Ti;
    float Td;

    float P;
    float I;
    float D;
    
    float last_e;
    float int_e;
    
    unsigned long int last_time;
};


#endif
