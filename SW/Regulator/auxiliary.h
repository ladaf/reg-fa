#include "const.h"

void setPinModes(){  
  pinMode(SW_A_pin, INPUT_PULLUP);
  pinMode(SW_B_pin, INPUT_PULLUP);
  pinMode(SW_C_pin, INPUT_PULLUP);
  
  pinMode(JMP_A_pin, INPUT_PULLUP);
  pinMode(JMP_B_pin, INPUT_PULLUP);
  pinMode(JMP_C_pin, INPUT_PULLUP);

  pinMode(LAYER_CURR_PWM_pin, OUTPUT);
}
