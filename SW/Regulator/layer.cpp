#include "layer.h"
#include "const.h"

float Layer::readShuntVoltage(){
  return (5./1023*analogRead(I_SENS_pin));
}


float Layer::readLayerVoltage(){
  float pinVoltage = 5./1023*analogRead(U_SENS_pin)/layer_division_coeff;
  return (pinVoltage-readShuntVoltage());
}

float Layer::readCurrent(){
  return (readShuntVoltage()/layer_shunt);
}

float Layer::readResistance(){
  return (readLayerVoltage()/readCurrent());
}
