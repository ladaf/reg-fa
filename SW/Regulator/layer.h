#ifndef layer_h
#define layer_h

class Layer{
  public:
    float readLayerVoltage();
    float readShuntVoltage();
    float readCurrent();
    float readResistance();
  private:
  
};

#endif
