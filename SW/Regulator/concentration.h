#ifndef concentration_h
#define concentration_h

#include "const.h"

float concentration(float r) {
  float result = concA*exp(r-1)-concA;
  if(result<-1.)
    return NAN;
  if (result<0)
    return 0;
  return result;
}

#endif
