#include "pid.h"
#include <arduino.h>

void PID_regulator::setW(float new_w) {
  this->w = new_w;
}

void PID_regulator::setParameters(float new_r0, float new_Ti, float new_Td) {
  r0 = new_r0;
  Ti = new_Ti;
  Td = new_Td;
}

float PID_regulator::evaluate(float new_y) {
  //ulozim si novou hodnotu namerene veliciny
  y = new_y;

  //nejprve me zajima casova_perioda
  unsigned long int t_current = micros();
  
  //aktualni periodu spcitam v sekundach
  float t = ((float)(t_current - last_time)) / 1000000;
  last_time = t_current;
  
  //spoctu si regulacni odchylku
  float e = w - y;

  //a napocitam si derivaci a integral
  float der_e = t * (e - last_e);
  last_e = e;
  int_e = int_e + t * e;
  
  //ted zalimituji
  if (int_e * (r0/Ti) > 1)
    int_e = Ti/r0;
  if (int_e * (r0/Ti) < -1)
    int_e = -Ti/r0;

  if (r0 * e > 1)
    e = 1/r0;
  if (r0 * e < -1)
    e = -1/r0;

    
/*
  if (der_e * (r0*Td) > 1)
    der_e = 1/(Td*r0);
  if (der_e * (r0*Td) < 1)
    der_e = -1/(Td*r0);
*/
  //ted si dopoctu parametry P, I a D
  //prepocitam podle koeficientu regulatoru
  P = r0 * e;
  I = (r0 / Ti) * int_e;
  D = (r0 * Td) * der_e;

  v = P + I + D;

  if (v>1)
    v=1;
  if (v<0)
    v=0;
  
  return v;
}

void PID_regulator::debug(byte level) {
  if (level == 0) {
    Serial.print(w); Serial.print("\t");
    Serial.print(y); Serial.print("\t");
    Serial.print(P); Serial.print("\t");
    Serial.print(I); Serial.print("\t");
    Serial.print(D); Serial.print("\t");
    Serial.println(v);// Serial.print("\t");
  }
  if (level == 1) {
    Serial.print(w); Serial.print("\t");
    Serial.print(y); Serial.print("\t");
    Serial.print(v); Serial.print("\t");
  }
  if (level == 2) {
    Serial.print(w); Serial.print("\t");
    Serial.print(y); Serial.print("\t");
  }
}

PID_regulator::PID_regulator() {
  last_time = 0;
  last_e = 0;
  int_e = 0;
}
