#include "ohmmeter.h"
#include "const.h"
//#include <SDL_Arduino_INA3221.h> 

float INA_ohmmeter::getVoltage(){
  return U_cal * getBusVoltage_V(INA_ch);
}

float INA_ohmmeter::getCurrent(){
  return I_cal * getShuntVoltage_mV(INA_ch)/(1000.f*R_shunt);
}

float INA_ohmmeter::getResistance(){
  return getVoltage()/getCurrent();
}

void INA_ohmmeter::printDebugInfo(){
  Serial.print("U: ");
  Serial.print(getVoltage());
  
  Serial.print("\tI: ");
  Serial.print(getCurrent());

  Serial.print("\tR: ");
  Serial.println(getResistance());
}
