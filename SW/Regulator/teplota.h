#ifndef teplota_h
#define teplota_h

#include "const.h"

float temperature(float r) {
  #define A 3.983e-3
  #define B -5.775e-7
  
  float D = A * A - 4 * B * (1 - (r / R0));  
  return (-A + sqrt(D)) / (2 * B);
}

float normalize(float t){
  return t/T_max;
}

float denormalize(float x){
  return x*T_max;
}

float potTempRead(){
  int pot = analogRead(W_pin);
  return (float)pot*(T_max-T_min)/1023.f + T_min;
}

float serialTempRead(float t){
  if(Serial.available()){
      int serialIn = Serial.parseInt();
      if (serialIn <=T_max && serialIn >= T_min)
        return (float)serialIn;
  }
  return t;
}

#endif
