
#include "display.h"                // Vojta
#include "ohmmeter.h"                // Vojta
#include "teplota.h"                // Vojta

extern info_display disp;
extern Adafruit_MCP4725 dac;
extern INA_ohmmeter ohmmeter;

void loopCalibrationINA(){
  while(!digitalRead(JMP_A_pin)){
    disp.clearDisplay();
    disp.setTextSize(2);
    disp.setTextColor(WHITE);
    disp.setCursor(0, 0);
    disp.print("Kalib INA");
  

    int pot = analogRead(W_pin);
    int DACout = 4*pot;

    float U = ohmmeter.getVoltage();
    float I = 1000*ohmmeter.getCurrent();
    
    char dispPercent [6];
    dtostrf((float)pot*100.f/1023.f, 5, 1, dispPercent);
    char dispVoltage [6];
    dtostrf(U, 5, 3, dispVoltage);
    char dispCurrent [6];
    dtostrf(I, 5, 1, dispCurrent);


    disp.setCursor(50, 16);
    disp.print(dispPercent);
    disp.setCursor(116, 16);
    disp.print('%');

    disp.setCursor(0, 32);
    disp.print('U');
    disp.setCursor(20, 32);
    disp.print(dispVoltage);
    disp.setCursor(116, 32);
    disp.print('V');

    disp.setCursor(0, 48);
    disp.print('I');
    disp.setCursor(20, 48);
    disp.print(dispCurrent);
    disp.setCursor(104, 48);
    disp.print("mA");
    
    Serial.println(dispVoltage);
    dac.setVoltage(DACout, false);
    disp.display();
    delay(42);
  }
}


void loopCalibrationSensor(){
  while(!digitalRead(JMP_B_pin)){
    disp.clearDisplay();
    disp.setTextSize(2);
    disp.setTextColor(WHITE);
    disp.setCursor(0, 0);
    disp.print("Kalib Sens");
  

    int pot = analogRead(W_pin);
    int DACout = 4*pot;

    float R = ohmmeter.getResistance();
    float T = temperature(R);
    
    char dispPercent [6];
    dtostrf((float)pot*100.f/1023.f, 5, 1, dispPercent);
    char dispResist [6];
    dtostrf(R, 5, 2, dispResist);
    char dispTemp [6];
    dtostrf(T, 5, 1, dispTemp);


    disp.setCursor(50, 16);
    disp.print(dispPercent);
    disp.setCursor(116, 16);
    disp.print('%');

    disp.setCursor(0, 32);
    disp.print('R');
    disp.setCursor(20, 32);
    disp.print(dispResist);
    disp.setCursor(116, 32);
    disp.print('O');

    disp.setCursor(0, 48);
    disp.print('T');
    disp.setCursor(20, 48);
    disp.print(dispTemp);
    disp.setCursor(116, 48);
    disp.print("C");
    
    //Serial.println(dispVoltage);
    dac.setVoltage(DACout, false);
    disp.display();
    delay(42);
  }
}
