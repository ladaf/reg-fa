#ifndef display_h
#define display_h

#define OLED_ADDR   0x3C
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

class info_display:public Adafruit_SSD1306{
  private:
    float tempMax;

    void units(char, char, char, char);
    void units(char*, char*, char*, char*);
    void quantities(char*, char*, char*, char*);

  public:
    info_display() : Adafruit_SSD1306(128, 64, &Wire, -1) {}
    begin();
    
    standView(float, float, float, float);
    tSetView(float, float, float, float);
    
};

#endif
