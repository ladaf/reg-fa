/*
  pro ucely obvod DRASLOVKA - mereni proudu a napeti na
  topeni senzoru pro vypocet jeho teploty pomoci INA3221
  regulace primitivn9 +/- 1

  Příklad komunikace s INA3221 (monitorování napětí a proudu)
  https://navody.arduino-shop.cz/navody-k-produktum/senzor-napeti-a-proudu-ina3221.html
  na Arduinu je SDA = A4
                SCL = A5
  Merim ZATIM na shuntu 0,1 ohm - originalni

  Komunikace s DHT11 (teplota a vlhkost)
  je pripojen na pin 7  
  k němu je DHT sensor library (Adafruit)

  k zadani zadane hodnoty pouzivam analogovy vstup A0

  výpis do ladicího okna formátuji aby šel snadno přenést do excelu
*/

/*
 * Arduino Nano pinout
 * http://christianto.tjahyadi.com/wp-content/uploads/2014/11/nano.jpg
 * Arduino Uno pinout
 * http://foros.giltesa.com/otros/arduino/fc/docs/pinout/uno.jpg
 */

#include "display.h"                // Vojta
#include "ohmmeter.h"               // Vojta
#include "teplota.h"                // Vojta
#include "pid.h"                    // Vojta
#include "layer.h"
#include "concentration.h"
#include "DHT.h"                    // Adafruit
#include <Wire.h>                   // zakladni Arduino
#include <Adafruit_MCP4725.h>       // Adafruit
#include "const.h"                  //globalni konfiguracni konstanty
#include "auxiliary.h"              //pomocne funkce
#include "calibration.h"


//objekt pro INA senzor
INA_ohmmeter ohmmeter;


// vytvoření objektu senzoru DHT11 z knihovny

#define DHTTYPE DHT22
DHT dht(DHT_pin, DHTTYPE);

//vytvoření objektu externi DAC z knihovny
Adafruit_MCP4725 dac;

//vytvoření objektu pro diplej 
info_display disp;

//vytvoření objektu pro regulator 
PID_regulator pid;


Layer layer;

// vytvoření měřicích proměnných
float napetiSenzor;
//float napetiBocnik;
float proudZatez;
//float napetiZatez;
//float napetiSkutecne;
//float proudSkutecny;
//float odporTopeni;
float vlhkost;
float teplotaKomory;
float teplotaSenzoru;
int ZadHoA0;
float ZadHoPrp;
int DACout;
float odporSenzoru;
float Rpoz;
float Rmi1;

float Tpoz_norm;
float Tmer_norm;
float regulator_output;
float Tpoz = 150;

float rVzd;

void setup() {
  Serial.begin(9600);
  ohmmeter.begin();       // tohle je voltmetr a ampermetr
  dht.begin();           // tohle je k VLHKOSTI
  dac.begin(0x60);       // tohle je k DAC
  disp.begin();          // tohle inicializuje displej
  pid.setParameters(r0, Ti, Td);
  
  // vytvoreni hlavicky vypisu
  //ladeni PID regulatoru
  Serial.println("Tpoz\tTskut");
  //kalibrace INA
  //Serial.println("U /V\tI /mA\tR /ohm");
  
  // DACout = 50;         // startovni vystup
  Rmi1 = 17;              // vychozi hodnota pro filtr

  setPinModes();
  if(!digitalRead(JMP_A_pin))
    loopCalibrationINA();
  if(!digitalRead(JMP_B_pin))
    loopCalibrationSensor();
}


void loop() {
  byte buttB = digitalRead(SW_B_pin);
  byte buttC = digitalRead(SW_C_pin);

  // Cteni zadane hodnoty
  if(buttB){
    Tpoz = potTempRead();
    disp.tSetView(Tpoz, teplotaSenzoru, (float)vlhkost, teplotaKomory);
  }
  else{
    Tpoz = serialTempRead(Tpoz);
    disp.standView(concentration(rVzd/layer.readResistance()), rVzd, layer.readResistance(), (float)vlhkost);  
  }
  Tpoz_norm = normalize(Tpoz);
  
  if(!buttC)
    rVzd = layer.readResistance();

  // prace s odporem topeni
  odporSenzoru = filter_c * (ohmmeter.getResistance()) + (1 - filter_c) * Rmi1;
  Rmi1 = odporSenzoru;
  teplotaSenzoru = temperature(odporSenzoru);

  
  Tmer_norm = normalize(teplotaSenzoru);

  pid.setW(Tpoz_norm);


  regulator_output = pid.evaluate(Tmer_norm);
  pid.debug(3);
  Serial.print(Tpoz); Serial.print("\t");
  Serial.println(teplotaSenzoru); //Serial.print("\t");
  /*Serial.print(layer.readLayerVoltage()); Serial.print("\t");
  Serial.print(layer.readCurrent()*1000.f); Serial.print("\t");
  Serial.println(layer.readResistance()); //Serial.print("\t");
  */
  vlhkost = dht.readHumidity();
  teplotaKomory = dht.readTemperature();

  

 //Serial.print(teplotaKomory); Serial.print("\t");
 //Serial.print(vlhkost); Serial.print("\n");


  float Uin = power_voltage_coeff * analogRead(PWR_U_pin);
  DACout = (int)(regulator_output * 4095);
  if(Uin>=power_voltage_limit){
    dac.setVoltage(DACout, false);
    digitalWrite(LED_BUILTIN, LOW);
  }
  else{
    dac.setVoltage(1, false);
    digitalWrite(LED_BUILTIN, HIGH);
  }
  delay(loop_delay);

}
