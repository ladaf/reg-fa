EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Temperature Regulator for chemiresistor"
Date "2020-12-26"
Rev "3"
Comp "VŠCHT v Praze"
Comment1 "Fišer"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator-rescue:MCP4725_Module-My_symbols DAC1
U 1 1 5F7B2FE0
P 7750 4150
F 0 "DAC1" H 8078 4296 50  0000 L CNN
F 1 "MCP4725_Module" H 8078 4205 50  0000 L CNN
F 2 "Modules_for_Regulator:DAC_module_board" H 7600 4800 50  0001 C CNN
F 3 "" H 7600 4800 50  0001 C CNN
	1    7750 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3700 5850 3900
Wire Wire Line
	5900 3600 5900 4000
$Comp
L Device:R R2
U 1 1 5F7B461D
P 6500 3350
F 0 "R2" H 6570 3396 50  0000 L CNN
F 1 "R" H 6570 3305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6430 3350 50  0001 C CNN
F 3 "~" H 6500 3350 50  0001 C CNN
	1    6500 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5F7B4E35
P 6200 3350
F 0 "R1" H 6270 3396 50  0000 L CNN
F 1 "R" H 6270 3305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6130 3350 50  0001 C CNN
F 3 "~" H 6200 3350 50  0001 C CNN
	1    6200 3350
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR03
U 1 1 5F7B539F
P 6500 3200
F 0 "#PWR03" H 6500 3050 50  0001 C CNN
F 1 "+5V" H 6515 3373 50  0000 C CNN
F 2 "" H 6500 3200 50  0001 C CNN
F 3 "" H 6500 3200 50  0001 C CNN
	1    6500 3200
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR02
U 1 1 5F7B596F
P 6200 3200
F 0 "#PWR02" H 6200 3050 50  0001 C CNN
F 1 "+5V" H 6215 3373 50  0000 C CNN
F 2 "" H 6200 3200 50  0001 C CNN
F 3 "" H 6200 3200 50  0001 C CNN
	1    6200 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 5F7B68F3
P 7750 3150
F 0 "J1" H 7778 3126 50  0000 L CNN
F 1 "DAC_out_debug" H 7778 3035 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 7750 3150 50  0001 C CNN
F 3 "~" H 7750 3150 50  0001 C CNN
	1    7750 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3850 6850 3850
Wire Wire Line
	6850 3850 6850 3150
Wire Wire Line
	7050 3250 7050 3750
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR04
U 1 1 5F7B9ED0
P 7050 4250
F 0 "#PWR04" H 7050 4100 50  0001 C CNN
F 1 "+5V" V 7065 4378 50  0000 L CNN
F 2 "" H 7050 4250 50  0001 C CNN
F 3 "" H 7050 4250 50  0001 C CNN
	1    7050 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 3700 6200 3700
Wire Wire Line
	6200 3500 6200 3700
Connection ~ 6200 3700
Wire Wire Line
	5900 3600 6500 3600
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 5F7BD0EA
P 5650 1900
F 0 "J2" V 5650 2100 50  0000 R CNN
F 1 "12Vin" V 5550 2200 50  0000 R CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MC_1,5_2-G-5.08_1x02_P5.08mm_Horizontal" H 5650 1900 50  0001 C CNN
F 3 "~" H 5650 1900 50  0001 C CNN
	1    5650 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 2100 5650 2150
Wire Wire Line
	6200 3700 6700 3700
Wire Wire Line
	6500 3500 6500 3600
Connection ~ 6500 3600
Wire Wire Line
	6500 3600 6650 3600
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR08
U 1 1 5F7D3877
P 3700 5500
F 0 "#PWR08" H 3700 5250 50  0001 C CNN
F 1 "GND" H 3705 5327 50  0000 C CNN
F 2 "" H 3700 5500 50  0001 C CNN
F 3 "" H 3700 5500 50  0001 C CNN
	1    3700 5500
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:Arduino_Nano_v3.x-MCU_Module-regulator2-rescue A1
U 1 1 5F7AF2FA
P 3600 3500
F 0 "A1" H 3600 2411 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 3600 3050 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 3600 3500 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 3600 3500 50  0001 C CNN
	1    3600 3500
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR014
U 1 1 5F7D5B19
P 7050 4350
F 0 "#PWR014" H 7050 4100 50  0001 C CNN
F 1 "GND" V 7055 4222 50  0000 R CNN
F 2 "" H 7050 4350 50  0001 C CNN
F 3 "" H 7050 4350 50  0001 C CNN
	1    7050 4350
	0    1    1    0   
$EndComp
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR012
U 1 1 5F7D64BD
P 5650 2200
F 0 "#PWR012" H 5650 1950 50  0001 C CNN
F 1 "GND" H 5655 2027 50  0000 C CNN
F 2 "" H 5650 2200 50  0001 C CNN
F 3 "" H 5650 2200 50  0001 C CNN
	1    5650 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10950 1650 10950 2700
Wire Wire Line
	10950 2700 10650 2700
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR020
U 1 1 5F7E194C
P 10200 2200
F 0 "#PWR020" H 10200 1950 50  0001 C CNN
F 1 "GND" H 10205 2027 50  0000 C CNN
F 2 "" H 10200 2200 50  0001 C CNN
F 3 "" H 10200 2200 50  0001 C CNN
	1    10200 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 2200 10350 2200
Wire Wire Line
	10350 2200 10350 2400
$Comp
L Connector:DIN-5_180degree J9
U 1 1 5F7E2C97
P 10350 2700
F 0 "J9" H 10350 2425 50  0000 C CNN
F 1 "DIN-5_180degree" H 10350 2334 50  0000 C CNN
F 2 "Modules_for_Regulator:DIN-5" H 10350 2700 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 10350 2700 50  0001 C CNN
	1    10350 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J12
U 1 1 5F7E657E
P 10550 1900
F 0 "J12" V 10488 1712 50  0000 R CNN
F 1 "Vrstva" V 10397 1712 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 10550 1900 50  0001 C CNN
F 3 "~" H 10550 1900 50  0001 C CNN
	1    10550 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10650 2100 10650 2600
Wire Wire Line
	10550 2100 9900 2100
Wire Wire Line
	9900 2100 9900 2700
Wire Wire Line
	9900 2700 10050 2700
$Comp
L Connector:Conn_01x02_Female J6
U 1 1 5F7E99F4
P 4600 600
F 0 "J6" V 4538 412 50  0000 R CNN
F 1 "DCDC Pwr out" V 4447 412 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4600 600 50  0001 C CNN
F 3 "~" H 4600 600 50  0001 C CNN
	1    4600 600 
	0    -1   -1   0   
$EndComp
$Comp
L Regulator-rescue:DC-DC-zdroj-Moje_symboly DC1
U 1 1 5F7EAA09
P 5600 1150
F 0 "DC1" H 5978 1401 50  0000 L CNN
F 1 "DC-DC-zdroj" H 5978 1310 50  0000 L CNN
F 2 "Modules_for_Regulator:DC-DC_convertor" H 5600 850 50  0001 C CNN
F 3 "" H 5600 850 50  0001 C CNN
	1    5600 1150
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR09
U 1 1 5F7EBDAD
P 4250 850
F 0 "#PWR09" H 4250 700 50  0001 C CNN
F 1 "+5V" H 4265 1023 50  0000 C CNN
F 2 "" H 4250 850 50  0001 C CNN
F 3 "" H 4250 850 50  0001 C CNN
	1    4250 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 850  4600 850 
Wire Wire Line
	4600 850  4600 800 
Wire Wire Line
	4700 800  5500 800 
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR013
U 1 1 5F7EDFA0
P 5250 900
F 0 "#PWR013" H 5250 650 50  0001 C CNN
F 1 "GND" H 5255 727 50  0000 C CNN
F 2 "" H 5250 900 50  0001 C CNN
F 3 "" H 5250 900 50  0001 C CNN
	1    5250 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 900  5500 900 
Wire Wire Line
	5400 1000 5500 1000
$Comp
L Regulator-rescue:0.96_OLED_I2C_display-Moje_symboly I2C_disp1
U 1 1 5F7F160B
P 7250 5300
F 0 "I2C_disp1" H 7628 5551 50  0000 L CNN
F 1 "0.96_OLED_I2C_display" H 7628 5460 50  0000 L CNN
F 2 "Modules_for_Regulator:I2C_Display" H 7250 5000 50  0001 C CNN
F 3 "" H 7250 5000 50  0001 C CNN
	1    7250 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 5050 7150 5050
Wire Wire Line
	6650 3600 6650 4050
Connection ~ 6650 3600
Wire Wire Line
	6650 4050 7050 4050
Wire Wire Line
	6700 3700 6700 4150
Wire Wire Line
	6700 4950 7150 4950
Connection ~ 6700 3700
Wire Wire Line
	6700 4150 7050 4150
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR015
U 1 1 5F7F55F4
P 7150 5150
F 0 "#PWR015" H 7150 5000 50  0001 C CNN
F 1 "+5V" V 7165 5278 50  0000 L CNN
F 2 "" H 7150 5150 50  0001 C CNN
F 3 "" H 7150 5150 50  0001 C CNN
	1    7150 5150
	0    -1   -1   0   
$EndComp
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR016
U 1 1 5F7F6501
P 7150 5250
F 0 "#PWR016" H 7150 5000 50  0001 C CNN
F 1 "GND" H 7155 5077 50  0000 C CNN
F 2 "" H 7150 5250 50  0001 C CNN
F 3 "" H 7150 5250 50  0001 C CNN
	1    7150 5250
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR011
U 1 1 5F7FC4D5
P 5550 7300
F 0 "#PWR011" H 5550 7050 50  0001 C CNN
F 1 "GND" H 5555 7127 50  0000 C CNN
F 2 "" H 5550 7300 50  0001 C CNN
F 3 "" H 5550 7300 50  0001 C CNN
	1    5550 7300
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR010
U 1 1 5F7FDE0E
P 5550 7100
F 0 "#PWR010" H 5550 6950 50  0001 C CNN
F 1 "+5V" H 5565 7273 50  0000 C CNN
F 2 "" H 5550 7100 50  0001 C CNN
F 3 "" H 5550 7100 50  0001 C CNN
	1    5550 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2100 5900 2100
Connection ~ 5750 2100
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR05
U 1 1 5F815691
P 900 5100
F 0 "#PWR05" H 900 4850 50  0001 C CNN
F 1 "GND" H 905 4927 50  0000 C CNN
F 2 "" H 900 5100 50  0001 C CNN
F 3 "" H 900 5100 50  0001 C CNN
	1    900  5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  4800 900  4900
Wire Wire Line
	1200 4900 900  4900
Connection ~ 900  4900
Wire Wire Line
	900  4900 900  5000
Wire Wire Line
	1500 5000 900  5000
Connection ~ 900  5000
Wire Wire Line
	900  5000 900  5100
$Comp
L Connector:Conn_01x15_Female J4
U 1 1 5F83BC2D
P 2150 5850
F 0 "J4" H 2042 4925 50  0000 C CNN
F 1 "Conn_01x15_Female" H 2042 5016 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 2150 5850 50  0001 C CNN
F 3 "~" H 2150 5850 50  0001 C CNN
	1    2150 5850
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 4100 3000 4100
Wire Wire Line
	3000 4100 3000 6550
Wire Wire Line
	3100 4000 2950 4000
Wire Wire Line
	2950 4000 2950 4700
Wire Wire Line
	3100 3900 2900 3900
Wire Wire Line
	2900 3900 2900 4600
Wire Wire Line
	3100 3800 2850 3800
Wire Wire Line
	2800 3700 3100 3700
Wire Wire Line
	3100 3600 2750 3600
Wire Wire Line
	2750 3600 2750 6050
Wire Wire Line
	2700 5950 2700 3500
Wire Wire Line
	2700 3500 3100 3500
Wire Wire Line
	3100 3400 2650 3400
Wire Wire Line
	2650 3400 2650 5850
Wire Wire Line
	2350 5750 2600 5750
Wire Wire Line
	2600 5750 2600 3300
Wire Wire Line
	2600 3300 3100 3300
Wire Wire Line
	2550 3200 2550 5650
Wire Wire Line
	2550 5650 2350 5650
Wire Wire Line
	2350 5550 2500 5550
Wire Wire Line
	2500 5550 2500 3100
Wire Wire Line
	2500 3100 3100 3100
Wire Wire Line
	3600 4500 3600 5250
Wire Wire Line
	3600 5450 2350 5450
Wire Wire Line
	4100 3000 4150 3000
Wire Wire Line
	4150 3000 4150 5350
Wire Wire Line
	4150 5350 2350 5350
Wire Wire Line
	3100 3000 2400 3000
Wire Wire Line
	2400 3000 2400 5150
Wire Wire Line
	2400 5150 2350 5150
Wire Wire Line
	3100 2900 2450 2900
Wire Wire Line
	2450 2900 2450 5250
Wire Wire Line
	2450 5250 2350 5250
Wire Wire Line
	2350 5850 2650 5850
Wire Wire Line
	2350 5950 2700 5950
Wire Wire Line
	2350 6050 2750 6050
Wire Wire Line
	2350 6150 2800 6150
Wire Wire Line
	2350 6250 2850 6250
Wire Wire Line
	2350 6350 2900 6350
Wire Wire Line
	2350 6450 2950 6450
Wire Wire Line
	2350 6550 3000 6550
Wire Wire Line
	2250 3900 2250 3600
Wire Wire Line
	2250 3600 2750 3600
Connection ~ 2750 3600
Wire Wire Line
	900  4400 2800 4400
Wire Wire Line
	2850 4500 1200 4500
Wire Wire Line
	1500 4600 2900 4600
$Comp
L Connector:Conn_01x15_Female J8
U 1 1 5F8E9ABC
P 5550 5850
F 0 "J8" H 5578 5876 50  0000 L CNN
F 1 "Conn_01x15_Female" H 5578 5785 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 5550 5850 50  0001 C CNN
F 3 "~" H 5550 5850 50  0001 C CNN
	1    5550 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4200 3100 6550
Wire Wire Line
	3100 6550 5350 6550
Wire Wire Line
	3700 2500 3700 2250
Wire Wire Line
	3700 2250 4200 2250
Wire Wire Line
	4200 2250 4200 6450
Wire Wire Line
	4200 6450 5350 6450
Wire Wire Line
	4100 3300 4250 3300
Wire Wire Line
	4250 3300 4250 6350
Wire Wire Line
	4250 6350 5350 6350
Wire Wire Line
	4100 3500 4300 3500
Wire Wire Line
	4300 6250 5350 6250
Wire Wire Line
	4300 3500 4300 6250
Wire Wire Line
	4100 3600 4350 3600
Wire Wire Line
	4350 3600 4350 6150
Wire Wire Line
	4350 6150 5350 6150
Wire Wire Line
	5350 6050 4400 6050
Wire Wire Line
	4400 6050 4400 3700
Wire Wire Line
	4400 3700 4100 3700
Wire Wire Line
	4100 3800 4450 3800
Wire Wire Line
	4450 3800 4450 5950
Wire Wire Line
	4450 5950 5350 5950
Wire Wire Line
	5350 5850 4500 5850
Wire Wire Line
	4500 5850 4500 3900
Wire Wire Line
	4500 3900 4100 3900
Wire Wire Line
	4100 4000 4550 4000
Wire Wire Line
	4550 4000 4550 5750
Wire Wire Line
	4550 5750 5350 5750
Wire Wire Line
	5350 5650 4600 5650
Wire Wire Line
	4600 5650 4600 4550
Wire Wire Line
	4600 4100 4100 4100
Wire Wire Line
	4100 4200 4650 4200
Wire Wire Line
	4650 4200 4650 4600
Wire Wire Line
	4650 5550 5350 5550
Wire Wire Line
	3800 2500 4700 2500
Wire Wire Line
	4700 2500 4700 5450
Wire Wire Line
	4700 5450 5350 5450
Wire Wire Line
	4100 2900 4750 2900
Wire Wire Line
	4750 2900 4750 5350
Wire Wire Line
	4750 5350 5350 5350
Wire Wire Line
	3700 5250 5350 5250
Wire Wire Line
	3700 5250 3700 5500
Wire Wire Line
	3700 4500 3700 5250
Connection ~ 3700 5250
Wire Wire Line
	3500 2500 3500 2400
Wire Wire Line
	4800 2400 4800 5150
Wire Wire Line
	4800 5150 5350 5150
Wire Wire Line
	4300 3500 5150 3500
Connection ~ 4300 3500
$Comp
L Connector:Conn_01x02_Female J5
U 1 1 5F990161
P 4800 1350
F 0 "J5" V 4750 1750 50  0000 R CNN
F 1 "Int stabiliser in" V 4600 2300 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4800 1350 50  0001 C CNN
F 3 "~" H 4800 1350 50  0001 C CNN
	1    4800 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4900 1550 5400 1550
Wire Wire Line
	4500 3900 5850 3900
Connection ~ 4500 3900
Wire Wire Line
	4550 4000 5900 4000
Connection ~ 4550 4000
$Comp
L Connector:DIN-5_180degree J11
U 1 1 5F9CEDE6
P 10400 3700
F 0 "J11" H 10400 3425 50  0000 C CNN
F 1 "DIN-5_180degree" H 10400 3334 50  0000 C CNN
F 2 "Modules_for_Regulator:DIN-5" H 10400 3700 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 10400 3700 50  0001 C CNN
	1    10400 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 2700 9900 3700
Wire Wire Line
	9900 3700 10100 3700
Connection ~ 9900 2700
Wire Wire Line
	10650 2600 10850 2600
Wire Wire Line
	10850 2600 10850 3600
Wire Wire Line
	10850 3600 10700 3600
Connection ~ 10650 2600
Wire Wire Line
	10950 2700 10950 3700
Wire Wire Line
	10950 3700 10700 3700
Connection ~ 10950 2700
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR021
U 1 1 5F9F3539
P 10650 3250
F 0 "#PWR021" H 10650 3000 50  0001 C CNN
F 1 "GND" H 10655 3077 50  0000 C CNN
F 2 "" H 10650 3250 50  0001 C CNN
F 3 "" H 10650 3250 50  0001 C CNN
	1    10650 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 3400 10400 3250
Wire Wire Line
	10400 3250 10650 3250
$Comp
L Connector:Conn_01x02_Female J10
U 1 1 5F9FEA20
P 10450 1450
F 0 "J10" V 10650 1600 50  0000 R CNN
F 1 "Output_curr_prob" V 10550 1700 50  0000 R CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MC_1,5_2-G-5.08_1x02_P5.08mm_Horizontal" H 10450 1450 50  0001 C CNN
F 3 "~" H 10450 1450 50  0001 C CNN
	1    10450 1450
	0    1    -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J13
U 1 1 5FA1B743
P 10800 850
F 0 "J13" V 10800 1050 50  0000 R CNN
F 1 "Output_voltage" V 10650 1550 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 10800 850 50  0001 C CNN
F 3 "~" H 10800 850 50  0001 C CNN
	1    10800 850 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10800 1050 10800 1650
Connection ~ 10800 1650
Wire Wire Line
	10800 1650 10950 1650
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR022
U 1 1 5FA2ED3D
P 10900 1050
F 0 "#PWR022" H 10900 800 50  0001 C CNN
F 1 "GND" H 10905 877 50  0000 C CNN
F 2 "" H 10900 1050 50  0001 C CNN
F 3 "" H 10900 1050 50  0001 C CNN
	1    10900 1050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male J7
U 1 1 5FA37D00
P 5950 7200
F 0 "J7" H 5922 7132 50  0000 R CNN
F 1 "w_pot" H 6550 7650 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 5950 7200 50  0001 C CNN
F 3 "~" H 5950 7200 50  0001 C CNN
	1    5950 7200
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 7100 5750 7100
Wire Wire Line
	5750 7000 5750 7100
Connection ~ 5750 7100
Wire Wire Line
	5750 7400 5750 7300
Wire Wire Line
	5550 7300 5750 7300
Connection ~ 5750 7300
Wire Wire Line
	3500 2400 4800 2400
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR01
U 1 1 5F7B5F94
P 5050 2500
F 0 "#PWR01" H 5050 2350 50  0001 C CNN
F 1 "+5V" H 5065 2673 50  0000 C CNN
F 2 "" H 5050 2500 50  0001 C CNN
F 3 "" H 5050 2500 50  0001 C CNN
	1    5050 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2400 3500 1750
Wire Wire Line
	3500 1750 4800 1750
Wire Wire Line
	4800 1750 4800 1550
Connection ~ 3500 2400
Wire Wire Line
	5050 2500 4700 2500
Connection ~ 4700 2500
Wire Wire Line
	5300 2100 5450 2100
Wire Wire Line
	5450 2100 5450 2150
Wire Wire Line
	5450 2150 5650 2150
Connection ~ 5650 2150
Wire Wire Line
	5650 2150 5650 2200
Wire Wire Line
	5400 1550 5400 1900
Wire Wire Line
	5400 1900 5300 1900
Connection ~ 5400 1550
Wire Wire Line
	5400 1550 5900 1550
$Comp
L Connector:Barrel_Jack_Switch J14
U 1 1 5FABE35E
P 4400 2050
F 0 "J14" H 4457 2367 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 4457 2276 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 4450 2010 50  0001 C CNN
F 3 "~" H 4450 2010 50  0001 C CNN
	1    4400 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1950 5300 1950
Wire Wire Line
	5300 1950 5300 1900
Wire Wire Line
	4700 2050 4700 2150
Wire Wire Line
	4700 2150 5300 2150
Wire Wire Line
	5300 2150 5300 2100
Connection ~ 4700 2150
$Comp
L Device:R R4
U 1 1 5FA42F6F
P 5950 2950
F 0 "R4" H 5880 2904 50  0000 R CNN
F 1 "8k2" H 5880 2995 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 2950 50  0001 C CNN
F 3 "~" H 5950 2950 50  0001 C CNN
	1    5950 2950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5FA45564
P 5950 3300
F 0 "R5" H 6020 3346 50  0000 L CNN
F 1 "4k7" H 6020 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 3300 50  0001 C CNN
F 3 "~" H 5950 3300 50  0001 C CNN
	1    5950 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5FA5E3EC
P 5950 3450
F 0 "#PWR026" H 5950 3200 50  0001 C CNN
F 1 "GND" H 5800 3400 50  0000 C CNN
F 2 "" H 5950 3450 50  0001 C CNN
F 3 "" H 5950 3450 50  0001 C CNN
	1    5950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3100 5950 3150
Wire Wire Line
	5950 3150 5450 3150
Wire Wire Line
	5450 3150 5450 3600
Wire Wire Line
	5450 3600 4350 3600
Connection ~ 5950 3150
Connection ~ 4350 3600
$Comp
L Amplifier_Operational:LM358 U1
U 1 1 5FE8C424
P 7650 2650
F 0 "U1" H 7850 2750 50  0000 C CNN
F 1 "LM358" H 7750 2850 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7650 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 7650 2650 50  0001 C CNN
	1    7650 2650
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM358 U1
U 2 1 5FE956FA
P 9600 5750
F 0 "U1" H 9600 5383 50  0000 C CNN
F 1 "LM358" H 9600 5474 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 9600 5750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 9600 5750 50  0001 C CNN
	2    9600 5750
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM358 U1
U 3 1 5FE9B2BB
P 6400 2400
F 0 "U1" H 6358 2446 50  0000 L CNN
F 1 "LM358" H 6358 2355 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 6400 2400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 6400 2400 50  0001 C CNN
	3    6400 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1550 5900 2100
Wire Wire Line
	5400 1000 5400 1550
Wire Wire Line
	5750 2100 5750 2500
Wire Wire Line
	9200 2500 8800 2500
Wire Wire Line
	9200 2450 9200 2500
$Comp
L Transistor_BJT:BD139 Q1
U 1 1 5F7BFF37
P 8700 2300
F 0 "Q1" V 9029 2300 50  0000 C CNN
F 1 "BD139" V 8938 2300 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-126-3_Vertical" H 8900 2225 50  0001 L CIN
F 3 "http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/CD00001225.pdf" H 8700 2300 50  0001 L CNN
	1    8700 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F800994
P 9200 2300
F 0 "R3" V 8993 2300 50  0000 C CNN
F 1 "560R" V 9084 2300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0617_L17.0mm_D6.0mm_P20.32mm_Horizontal" V 9130 2300 50  0001 C CNN
F 3 "~" H 9200 2300 50  0001 C CNN
	1    9200 2300
	-1   0    0    1   
$EndComp
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR018
U 1 1 5F7FA120
P 6900 1000
F 0 "#PWR018" H 6900 750 50  0001 C CNN
F 1 "GND" H 6750 950 50  0000 C CNN
F 2 "" H 6900 1000 50  0001 C CNN
F 3 "" H 6900 1000 50  0001 C CNN
	1    6900 1000
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR017
U 1 1 5F7F92E6
P 6900 900
F 0 "#PWR017" H 6900 750 50  0001 C CNN
F 1 "+5V" V 6915 1028 50  0000 L CNN
F 2 "" H 6900 900 50  0001 C CNN
F 3 "" H 6900 900 50  0001 C CNN
	1    6900 900 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6700 1250 6900 1250
Wire Wire Line
	7350 600  7350 750 
Wire Wire Line
	7050 600  7350 600 
$Comp
L Regulator-rescue:INA3221_black_module-Moje_symboly INA1
U 1 1 5F7DB975
P 7400 1350
F 0 "INA1" H 7475 827 50  0000 C CNN
F 1 "INA3221_black_module" H 7475 736 50  0000 C CNN
F 2 "Modules_for_Regulator:INA_3221_Black" H 7100 -100 50  0001 C CNN
F 3 "" H 7100 -100 50  0001 C CNN
	1    7400 1350
	1    0    0    -1  
$EndComp
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR019
U 1 1 5F7D6AAE
P 7050 600
F 0 "#PWR019" H 7050 350 50  0001 C CNN
F 1 "GND" H 7055 427 50  0000 C CNN
F 2 "" H 7050 600 50  0001 C CNN
F 3 "" H 7050 600 50  0001 C CNN
	1    7050 600 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1150 6900 1150
Wire Wire Line
	6650 1150 6650 3600
Wire Wire Line
	6700 1250 6700 3700
Connection ~ 6650 4050
Connection ~ 6700 4150
Wire Wire Line
	6700 4150 6700 4950
Wire Wire Line
	6650 4050 6650 5050
Wire Wire Line
	5750 2500 5950 2500
Wire Wire Line
	5950 2500 5950 2800
Wire Wire Line
	8800 2100 6300 2100
Connection ~ 5900 2100
Wire Wire Line
	7950 2650 8350 2650
Wire Wire Line
	8350 2650 8350 2300
Wire Wire Line
	8350 2300 8500 2300
Wire Wire Line
	7350 2550 7350 2300
Wire Wire Line
	7900 2300 7950 2300
Wire Wire Line
	7950 2300 7950 2650
Connection ~ 7950 2650
Wire Wire Line
	7350 2300 7600 2300
$Comp
L Device:R R8
U 1 1 6004D99A
P 7750 2300
F 0 "R8" V 7900 2250 50  0000 C CNN
F 1 "6K8" V 7900 2400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7680 2300 50  0001 C CNN
F 3 "~" H 7750 2300 50  0001 C CNN
	1    7750 2300
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 6007AA64
P 7100 2550
F 0 "R7" V 6893 2550 50  0000 C CNN
F 1 "10K" V 6984 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7030 2550 50  0001 C CNN
F 3 "~" H 7100 2550 50  0001 C CNN
	1    7100 2550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR029
U 1 1 6007CD63
P 6950 2550
F 0 "#PWR029" H 6950 2300 50  0001 C CNN
F 1 "GND" H 6955 2377 50  0000 C CNN
F 2 "" H 6950 2550 50  0001 C CNN
F 3 "" H 6950 2550 50  0001 C CNN
	1    6950 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2550 7350 2550
Connection ~ 7350 2550
Wire Wire Line
	6850 3150 7550 3150
Wire Wire Line
	7550 3250 7300 3250
Wire Wire Line
	7300 3250 7300 2750
Wire Wire Line
	7300 2750 7350 2750
Connection ~ 7300 3250
Wire Wire Line
	7300 3250 7050 3250
Wire Wire Line
	8800 2100 9200 2100
Wire Wire Line
	9200 2100 9200 2150
Connection ~ 8800 2100
Wire Wire Line
	8800 2500 8300 2500
Wire Wire Line
	8300 2500 8300 1550
Wire Wire Line
	8300 1550 8050 1550
Connection ~ 8800 2500
Connection ~ 6300 2100
Wire Wire Line
	6300 2100 6100 2100
$Comp
L power:GND #PWR027
U 1 1 600EDC61
P 6300 2700
F 0 "#PWR027" H 6300 2450 50  0001 C CNN
F 1 "GND" H 6305 2527 50  0000 C CNN
F 2 "" H 6300 2700 50  0001 C CNN
F 3 "" H 6300 2700 50  0001 C CNN
	1    6300 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 60109A3E
P 6700 5600
F 0 "R6" V 6907 5600 50  0000 C CNN
F 1 "R" V 6816 5600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6630 5600 50  0001 C CNN
F 3 "~" H 6700 5600 50  0001 C CNN
	1    6700 5600
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C1
U 1 1 6010B036
P 6850 5750
F 0 "C1" H 6965 5796 50  0000 L CNN
F 1 "C" H 6965 5705 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 6888 5600 50  0001 C CNN
F 3 "~" H 6850 5750 50  0001 C CNN
	1    6850 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM RV1
U 1 1 6010EF08
P 7450 5850
F 0 "RV1" H 7380 5896 50  0000 R CNN
F 1 "R_POT_TRIM" H 7380 5805 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA6-H2,5_Horizontal" H 7450 5850 50  0001 C CNN
F 3 "~" H 7450 5850 50  0001 C CNN
	1    7450 5850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J19
U 1 1 60111EBD
P 7700 6350
F 0 "J19" H 7350 6250 50  0000 C CNN
F 1 "Current_external_ref" H 7050 6400 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 7700 6350 50  0001 C CNN
F 3 "~" H 7700 6350 50  0001 C CNN
	1    7700 6350
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J20
U 1 1 60151B94
P 8250 5400
F 0 "J20" V 8312 5444 50  0000 L CNN
F 1 "CurSourceJ_pwm" V 8403 5444 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8250 5400 50  0001 C CNN
F 3 "~" H 8250 5400 50  0001 C CNN
	1    8250 5400
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J21
U 1 1 60153F59
P 8250 5650
F 0 "J21" V 8312 5694 50  0000 L CNN
F 1 "CurSourceJ_trim" V 8403 5694 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8250 5650 50  0001 C CNN
F 3 "~" H 8250 5650 50  0001 C CNN
	1    8250 5650
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J22
U 1 1 60155AF9
P 8250 5900
F 0 "J22" V 8312 5944 50  0000 L CNN
F 1 "CurSourceJ_ext" V 8403 5944 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8250 5900 50  0001 C CNN
F 3 "~" H 8250 5900 50  0001 C CNN
	1    8250 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	8250 5600 8350 5600
Wire Wire Line
	8350 5600 8350 5850
Wire Wire Line
	8350 5850 8250 5850
Wire Wire Line
	8350 5850 8350 6100
Wire Wire Line
	8350 6100 8250 6100
Connection ~ 8350 5850
Wire Wire Line
	8150 5600 6850 5600
Connection ~ 6850 5600
Wire Wire Line
	7600 5850 8150 5850
Wire Wire Line
	7900 6250 7900 6100
Wire Wire Line
	7900 6100 8150 6100
Wire Wire Line
	7900 6350 7900 6450
Wire Wire Line
	7900 6450 7450 6450
Wire Wire Line
	7450 6450 7450 6200
Wire Wire Line
	6850 5900 6850 6200
Wire Wire Line
	6850 6200 7450 6200
Connection ~ 7450 6200
Wire Wire Line
	7450 6200 7450 6000
$Comp
L power:GND #PWR028
U 1 1 601EC781
P 6850 6200
F 0 "#PWR028" H 6850 5950 50  0001 C CNN
F 1 "GND" H 6855 6027 50  0000 C CNN
F 2 "" H 6850 6200 50  0001 C CNN
F 3 "" H 6850 6200 50  0001 C CNN
	1    6850 6200
	1    0    0    -1  
$EndComp
Connection ~ 6850 6200
$Comp
L power:+5V #PWR030
U 1 1 601ED5A8
P 7450 5700
F 0 "#PWR030" H 7450 5550 50  0001 C CNN
F 1 "+5V" H 7465 5873 50  0000 C CNN
F 2 "" H 7450 5700 50  0001 C CNN
F 3 "" H 7450 5700 50  0001 C CNN
	1    7450 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 5600 6550 4700
Wire Wire Line
	6550 4700 2950 4700
Connection ~ 2950 4700
Wire Wire Line
	2950 4700 2950 6450
Wire Wire Line
	8350 5850 9300 5850
$Comp
L Device:R R9
U 1 1 602181F9
P 10450 5900
F 0 "R9" H 10520 5946 50  0000 L CNN
F 1 "R_I_sens (4K7)" H 10520 5855 50  0000 L CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MC_1,5_2-G-5.08_1x02_P5.08mm_Horizontal" V 10380 5900 50  0001 C CNN
F 3 "~" H 10450 5900 50  0001 C CNN
	1    10450 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR031
U 1 1 6021A3D8
P 10450 6050
F 0 "#PWR031" H 10450 5800 50  0001 C CNN
F 1 "GND" H 10455 5877 50  0000 C CNN
F 2 "" H 10450 6050 50  0001 C CNN
F 3 "" H 10450 6050 50  0001 C CNN
	1    10450 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5650 9250 5650
Wire Wire Line
	9250 5650 9250 5100
Wire Wire Line
	10450 5100 10450 5750
Wire Wire Line
	9250 5100 10450 5100
Wire Wire Line
	10850 3600 10850 5100
Wire Wire Line
	10850 5100 10450 5100
Connection ~ 10850 3600
Connection ~ 10450 5100
$Comp
L Connector:Conn_01x02_Male J23
U 1 1 60264ACD
P 8850 4900
F 0 "J23" V 8912 4944 50  0000 L CNN
F 1 "I_SensLayer" V 9003 4944 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8850 4900 50  0001 C CNN
F 3 "~" H 8850 4900 50  0001 C CNN
	1    8850 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	8850 5100 9250 5100
Connection ~ 9250 5100
Wire Wire Line
	8750 5100 8550 5100
Wire Wire Line
	8550 4600 4650 4600
Wire Wire Line
	8550 4600 8550 5100
Connection ~ 4650 4600
Wire Wire Line
	4650 4600 4650 5550
$Comp
L Connector:Conn_01x03_Male J18
U 1 1 602E4344
P 4050 7550
F 0 "J18" V 4204 7362 50  0000 R CNN
F 1 "w_select" V 4113 7362 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4050 7550 50  0001 C CNN
F 3 "~" H 4050 7550 50  0001 C CNN
	1    4050 7550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 7200 4150 7350
Wire Wire Line
	4150 7200 5750 7200
$Comp
L Connector:Conn_01x02_Female J17
U 1 1 602F4913
P 3150 7400
F 0 "J17" H 3042 7075 50  0000 C CNN
F 1 "w_external" H 3042 7166 50  0000 C CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MC_1,5_2-G-5.08_1x02_P5.08mm_Horizontal" H 3150 7400 50  0001 C CNN
F 3 "~" H 3150 7400 50  0001 C CNN
	1    3150 7400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 602F634C
P 3350 7400
F 0 "#PWR025" H 3350 7150 50  0001 C CNN
F 1 "GND" H 3355 7227 50  0000 C CNN
F 2 "" H 3350 7400 50  0001 C CNN
F 3 "" H 3350 7400 50  0001 C CNN
	1    3350 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 7300 3950 7300
Wire Wire Line
	3950 7300 3950 7350
Wire Wire Line
	5150 3500 5150 6900
Wire Wire Line
	5150 6900 4050 6900
Wire Wire Line
	4050 6900 4050 7350
Wire Wire Line
	2250 3900 1850 3900
$Comp
L Regulator-rescue:GND-power-regulator2-rescue #PWR06
U 1 1 5F7D6D28
P 1850 3800
F 0 "#PWR06" H 1850 3550 50  0001 C CNN
F 1 "GND" V 1855 3672 50  0000 R CNN
F 2 "" H 1850 3800 50  0001 C CNN
F 3 "" H 1850 3800 50  0001 C CNN
	1    1850 3800
	0    -1   -1   0   
$EndComp
$Comp
L Regulator-rescue:+5V-power-regulator2-rescue #PWR07
U 1 1 5F7D3142
P 1850 4000
F 0 "#PWR07" H 1850 3850 50  0001 C CNN
F 1 "+5V" V 1865 4128 50  0000 L CNN
F 2 "" H 1850 4000 50  0001 C CNN
F 3 "" H 1850 4000 50  0001 C CNN
	1    1850 4000
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5F7CC190
P 1650 3900
F 0 "J3" H 1758 4181 50  0000 C CNN
F 1 "DHT_sensor" H 1758 4090 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1650 3900 50  0001 C CNN
F 3 "~" H 1650 3900 50  0001 C CNN
	1    1650 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J16
U 1 1 5FF4E65D
P 650 3400
F 0 "J16" V 900 3400 50  0000 C CNN
F 1 "set_jmp1" V 1000 3300 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 650 3400 50  0001 C CNN
F 3 "~" H 650 3400 50  0001 C CNN
	1    650  3400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW3
U 1 1 5FFE99E3
P 1500 4800
F 0 "SW3" H 2100 4850 50  0000 R CNN
F 1 "SW_SPST" H 2250 4750 50  0000 R CNN
F 2 "Modules_for_Regulator:DIP-button" H 1500 4800 50  0001 C CNN
F 3 "~" H 1500 4800 50  0001 C CNN
	1    1500 4800
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5FFEAADF
P 1200 4700
F 0 "SW2" H 1700 4750 50  0000 R CNN
F 1 "SW_SPST" H 1850 4650 50  0000 R CNN
F 2 "Modules_for_Regulator:DIP-button" H 1200 4700 50  0001 C CNN
F 3 "~" H 1200 4700 50  0001 C CNN
	1    1200 4700
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 6002C10A
P 900 4600
F 0 "SW1" H 1300 4650 50  0000 R CNN
F 1 "SW_SPST" H 1450 4550 50  0000 R CNN
F 2 "Modules_for_Regulator:DIP-button" H 900 4600 50  0001 C CNN
F 3 "~" H 900 4600 50  0001 C CNN
	1    900  4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3700 5250 3600 5250
Connection ~ 3600 5250
Wire Wire Line
	3600 5250 3600 5450
Wire Wire Line
	2850 3800 2850 4500
Wire Wire Line
	2800 3700 2800 4400
Connection ~ 2800 4400
Wire Wire Line
	2800 4400 2800 6150
Connection ~ 2850 4500
Wire Wire Line
	2850 4500 2850 6250
Connection ~ 2900 4600
Wire Wire Line
	2900 4600 2900 6350
$Comp
L Connector:Conn_01x02_Male J24
U 1 1 60133001
P 1450 3600
F 0 "J24" V 1900 3650 50  0000 R CNN
F 1 "set_jmp3" V 2000 3650 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1450 3600 50  0001 C CNN
F 3 "~" H 1450 3600 50  0001 C CNN
	1    1450 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3100 3200 2550 3200
Connection ~ 2550 3200
Connection ~ 2600 3300
Wire Wire Line
	2650 3400 1550 3400
Connection ~ 2650 3400
$Comp
L Connector:Conn_01x02_Male J15
U 1 1 5FF4EF11
P 1050 3500
F 0 "J15" V 1400 3500 50  0000 C CNN
F 1 "set_jmp2" V 1500 3400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1050 3500 50  0001 C CNN
F 3 "~" H 1050 3500 50  0001 C CNN
	1    1050 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1150 3300 2600 3300
Wire Wire Line
	750  3200 2550 3200
Wire Wire Line
	650  3200 550  3200
Wire Wire Line
	550  3200 550  3600
Wire Wire Line
	1050 3300 950  3300
Wire Wire Line
	950  3300 950  3600
Wire Wire Line
	1450 3400 1350 3400
Wire Wire Line
	1350 3400 1350 3600
$Comp
L power:GND #PWR0101
U 1 1 603154D9
P 1350 3600
F 0 "#PWR0101" H 1350 3350 50  0001 C CNN
F 1 "GND" H 1355 3427 50  0000 C CNN
F 2 "" H 1350 3600 50  0001 C CNN
F 3 "" H 1350 3600 50  0001 C CNN
	1    1350 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 603168A8
P 950 3600
F 0 "#PWR0102" H 950 3350 50  0001 C CNN
F 1 "GND" H 955 3427 50  0000 C CNN
F 2 "" H 950 3600 50  0001 C CNN
F 3 "" H 950 3600 50  0001 C CNN
	1    950  3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 603174CF
P 550 3600
F 0 "#PWR0103" H 550 3350 50  0001 C CNN
F 1 "GND" H 555 3427 50  0000 C CNN
F 2 "" H 550 3600 50  0001 C CNN
F 3 "" H 550 3600 50  0001 C CNN
	1    550  3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 604782D0
P 6100 2400
F 0 "C2" H 6215 2446 50  0000 L CNN
F 1 "C" H 6215 2355 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 6138 2250 50  0001 C CNN
F 3 "~" H 6100 2400 50  0001 C CNN
	1    6100 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2250 6100 2100
Connection ~ 6100 2100
Wire Wire Line
	6100 2100 5900 2100
Wire Wire Line
	6300 2700 6100 2700
Wire Wire Line
	6100 2550 6100 2700
Connection ~ 6300 2700
Wire Wire Line
	10450 1650 10800 1650
Wire Wire Line
	8050 1650 10350 1650
$Comp
L Connector:Conn_01x02_Male J25
U 1 1 60531151
P 9350 4300
F 0 "J25" V 9412 4344 50  0000 L CNN
F 1 "U_SensLayer" V 9503 4344 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9350 4300 50  0001 C CNN
F 3 "~" H 9350 4300 50  0001 C CNN
	1    9350 4300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J26
U 1 1 605322F3
P 9700 4800
F 0 "J26" H 9808 4981 50  0000 C CNN
F 1 "Curr_source_to_layer" H 9808 4890 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9700 4800 50  0001 C CNN
F 3 "~" H 9700 4800 50  0001 C CNN
	1    9700 4800
	1    0    0    -1  
$EndComp
Connection ~ 9900 3700
Wire Wire Line
	9350 4500 9900 4500
Wire Wire Line
	9250 4500 8450 4500
Wire Wire Line
	8450 4500 8450 4550
Wire Wire Line
	8450 4550 4600 4550
Connection ~ 4600 4550
Wire Wire Line
	4600 4550 4600 4100
Wire Wire Line
	9900 3700 9900 4500
Wire Wire Line
	9900 4900 9900 5750
Wire Wire Line
	9900 4500 9900 4800
Connection ~ 9900 4500
$EndSCHEMATC
